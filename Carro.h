#ifndef CARRO_H
#define CARRO_H

#include <iostream>
#include <sstream>
#include "Piloto.h"
#include "Classificacao.h"
using namespace std;

enum class EstadoCarro{PARADO,MOVIMENTO,DANIFICADO,CRAZYPARADO};
static int CARROS = 0;


class Carro {
    string marca; // Campo obrigatório
    string modelo; // não é obrigatório, mas por defeito é colocado "Modelo Base"
    char identificador; //a-Z sendo que se houver mais que Z é colocado um ?
    float energia; //mAh
    float capacidadeMaxima; // capacidade maxima atual (pode ter a bateria viciada)
    float capacidadeInicial; //Capacidade maxima do carro quando ele é criado
    float velocidadeMaxima; // Velocidade maxima do carro definida aquando a criação do carro
    bool emergencia; //Sinal de emergencia pode ser ligado ou desligado idependentemente da velocidade
	EstadoCarro estado; //Enum com estado atual do carro
	Piloto *piloto;	// Ponteiro para o Piloto
	Classificacao* classificacao;
	int velocidade;
	bool terminei; // verifica se o carro ja terminou ou não a prova


public:
    Carro(string marca, float energia, float capacidade , float velocidadeMaxima, string modelo = "Modelo Base");
	~Carro();

	//gets
    string getMarca() const;
    string getModelo() const;
    char getIdentificador() const;
    string getEstadoText() const;
    string getAsString() const;
	string getAsString2() const;
    float getEnergia() const;
    float getCapacidadeMaxima() const;
    float getCapacidadeInicial() const;
    float getVelocidadeMaxima() const;
	bool getEmergencia() const;
	int getPosicao() const;
	int getLugar() const;
	Piloto* getPiloto() const;
	EstadoCarro getEstado() const;
	Classificacao* getClassificacao() const;
	int getVelocidade() const;
	bool getFinish() const;

	//sets
	void setLugar(int lugar);
	void setPosicao(int posicao);
	void setEstado(EstadoCarro e);
	void setEmergencia(bool em);
	void setPiloto(Piloto* piloto);
	void acelera();
	void trava(int);
	void velocidadeConstante();
	void setFinish(bool status);

	//func
    float carregar(float mah);
	void removePiloto();
	void avancaPosicao();
	void limpa(int i);
	void destroi();
	void gastaEnergia(); //mal

	bool operator==(const Carro& p2) const;
};



#endif //CARRO_H
