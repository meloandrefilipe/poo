#ifndef MAPA_H
#define MAPA_H

#include "DirecaoGeralViacao.h"
#include "Consola.h"
#include <iostream>
#include <sstream>
#include "Piloto.h"
#include "Autodromo.h"
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

using namespace std;
static int posCursor = 0;

class Mapa {
	DirecaoGeralViacao *dgv;
	int mode;
public:
	Mapa();
	void constroiMapa();
	int constroiGaragem(string nome, int posDoCursor);
	void constroiInterface();
	void trataComando(string comando);
	bool fileExists(const string& name);
	void lista();
	void listaCarros();
	int getMode();
	void setMode(int mode);
};

#endif // MAPA_H