class Carro;
#ifndef PILOTO_H
#define PILOTO_H

#include <iostream>
#include <vector>
#include <sstream>

using namespace std;
static int PILOTOS_REPETIDOS = 0;
enum class EstadoPiloto { CARRO, SEMCARRO};
enum class TipoPiloto { CRAZY, RAPIDO, SURPRESA, GENERICO };

class Piloto {
protected:
	string nome;
	EstadoPiloto estado; //Enum com estado atual do piloto
	TipoPiloto tipo;
	bool stop;
public:
	Piloto(string nome, vector<Piloto*>& lista);
	~Piloto();

	string getNome() const;
	string getEstadoText() const;
	string getTipoText() const;
	EstadoPiloto getEstado() const;
	TipoPiloto getTipo() const;
	string getAsString() const;
	bool getStop() const;
	void stopCarro(Carro* carro);
	void setStop();
	virtual int acelera(int segundo, int posicao, int carrosEmPista, Carro* carro);

	bool isOnCar() const;

	void setEstado(EstadoPiloto e);


	bool operator ==(const Piloto& p) {
		return this->nome == p.getNome();
	}
	bool operator ==(const string nome) {
		return this->nome == nome;
	}
};

class CrazyDriver : public Piloto {

	int segundosEmAtraso;
public:
	CrazyDriver(string nome, vector<Piloto*>&lista) : Piloto(nome, lista) {
		this->tipo = TipoPiloto::CRAZY;
		this->segundosEmAtraso = 0;
		this->stop = false;
	}
	~CrazyDriver();


	string getNome() const {
		return Piloto::getNome();
	}
	TipoPiloto getTipo() const{
		return Piloto::getTipo();
	}

	int acelera(int segundo, int posicao, int carrosEmPista, Carro* carro) override;
	

	int calculaSegundosDeAtraso(int segundos);
	bool calculaProbabilidadeDanificado() const;
};

class PilotoRapido : public Piloto {

	int contadorSegundos;
public:
	PilotoRapido(string nome, vector<Piloto*>& lista) : Piloto(nome, lista) {
		this->tipo = TipoPiloto::RAPIDO;
		contadorSegundos = 0;
		this->stop = false;
	}
	~PilotoRapido();

	string getNome() const {
		return Piloto::getNome();
	}
	TipoPiloto getTipo() const {
		return Piloto::getTipo();
	}

	int acelera(int segundo, int posicao, int carrosEmPista, Carro* carro) override;
	bool calculaAnsiedade() const;
};

class PilotoSurpresa : public Piloto {
	int contadorSegundos;
public:
	PilotoSurpresa(string nome, vector<Piloto*>& lista) : Piloto(nome, lista) {
		this->tipo = TipoPiloto::SURPRESA;
		contadorSegundos = 0;
		this->stop = false; 
	}
	~PilotoSurpresa();

	string getNome() const {
		return Piloto::getNome();
	}
	TipoPiloto getTipo() const {
		return Piloto::getTipo();
	}

	int acelera(int segundo, int posicao, int carrosEmPista, Carro* carro) override;

};

#endif //PILOTO_H
