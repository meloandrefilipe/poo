//
// Created by meloandrefilipe on 28/10/2019.
//

#include "Carro.h"
#include <iostream>
#include <sstream>

using namespace std;

Carro::Carro(string marca, float energia, float capacidade, float velocidadeMaxima, string modelo) {
    int ident = ('A' + CARROS);
    if(ident < 65 || ident >90){
        ident = 63;
    }

    this->marca = marca;
    this->modelo = modelo;
    this->energia = energia;
    this->capacidadeMaxima = capacidade;
    this->velocidadeMaxima = velocidadeMaxima;
    this->emergencia = false;
    this->estado = EstadoCarro::PARADO;
    this->identificador = (char)ident;
	this->piloto = nullptr;
	this->classificacao = new Classificacao();
	this->velocidade = 0;
	this->terminei = false;

    CARROS++;
}
Carro::~Carro()
{
	if (this->getPiloto() != nullptr) {
		this->getPiloto()->setEstado(EstadoPiloto::SEMCARRO);
	}
}
string Carro::getMarca() const {
    return this->marca;
}

string Carro::getModelo() const {
    return this->modelo;
}

bool Carro::getEmergencia() const {
    return this->emergencia;
}

bool Carro::getFinish() const {
	return this->terminei;
}

void Carro::setEmergencia(bool em){
	this->emergencia = em;
}
float Carro::getEnergia() const {
    return this->energia;
}

float Carro::getCapacidadeMaxima() const {
    return this->capacidadeMaxima;
}
float Carro::getCapacidadeInicial() const {
	return this->capacidadeInicial;
}

float Carro::getVelocidadeMaxima() const {
    return this->velocidadeMaxima;
}

string Carro::getEstadoText() const {
    ostringstream os;
    switch(this->getEstado()){
	case EstadoCarro::PARADO:
            os << "Carro parado!" << endl;
            break;
        case EstadoCarro::MOVIMENTO:
            os << "Carro em movimento!" << endl;
            break;
        case EstadoCarro::DANIFICADO:
            os << "Carro com danos!" << endl;
            break;
        case EstadoCarro::CRAZYPARADO:
            os << "Carro fora de serviço!" << endl;
            break;
    }
    return os.str();
}

EstadoCarro Carro::getEstado() const {
    return this->estado;
}

char Carro::getIdentificador() const {
    return this->identificador;
}

string Carro::getAsString() const {
    ostringstream os;
    os << "Carro " << this->getIdentificador() << ". Marca: " << this->getMarca() << ". Modelo: " << this->getModelo() << ". Energia: " <<this->getEnergia()<<" de "<<this->getCapacidadeMaxima() <<". Velocidade: " << this->getVelocidade() << " de " << this->getVelocidadeMaxima() << " k/h. Metro na pista: " << this->getClassificacao()->getPosicao() << "." ;
    return os.str();
}

string Carro::getAsString2() const {
	ostringstream os;
	os << "Carro " << this->getIdentificador() << ". Marca: " << this->getMarca() << ". Modelo: " << this->getModelo() << ". Energia: " << this->getEnergia() << " de " << this->getCapacidadeMaxima() << ".";
	return os.str();
}

float Carro::carregar(float mah) {
    float energia = this->getEnergia();
    float capacidadeAtual = this->getCapacidadeMaxima();

    if(this->getEstado() == EstadoCarro::PARADO) {
        if (energia + mah <= capacidadeAtual) {
            this->energia += mah;
            return this->getCapacidadeMaxima() - this->getEnergia();
        }else if(energia + mah > capacidadeAtual){
            this->energia = capacidadeAtual;
            return 0;
        }else if(energia == capacidadeAtual){
            return 0;
        }
    }
    return -1;
}

void Carro::setEstado(EstadoCarro e) {
    this->estado = e;
}

void Carro::setPiloto(Piloto *piloto) {
	this->piloto = piloto;
}

void Carro::removePiloto(){
	if (this->piloto != nullptr)
		this->piloto->setEstado(EstadoPiloto::SEMCARRO);
	this->piloto = nullptr;
}

Piloto* Carro::getPiloto() const{
	return this->piloto;
}

bool Carro::operator==(const Carro& p2) const {
    return this->getIdentificador() == p2.getIdentificador();
}

void Carro::destroi() {
	this->~Carro();
}
void Carro::avancaPosicao() {
	this->classificacao->avancaPosicao(this->getVelocidade());
}

int Carro::getPosicao() const{
	return this->classificacao->getPosicao();
}
void Carro::setLugar(int lugar) {
	this->classificacao->setLugar(lugar);
}

int Carro::getLugar() const {
	return this->classificacao->getLugar();
}

void Carro::limpa(int i) {
	this->classificacao->limpa(i);
	this->velocidade = 0;
	this->terminei = false;
	this->emergencia = false;
	this->estado = EstadoCarro::PARADO;
}

Classificacao* Carro::getClassificacao() const {
	return this->classificacao;
}

void Carro::acelera() {
	if (this->velocidade < this->velocidadeMaxima && this->energia > 0) {
		velocidade++;
		energia = energia - velocidade * 0.1;
		if (energia < 0)
			energia = 0;
	}
	if (energia == 0)
		trava(1);
}

void Carro::gastaEnergia() {
	if(energia > 0)
		energia = energia - velocidade * 0.1;
	if (energia <= 0) {
		trava(1);
		energia = 0;
	}
}

void Carro::trava(int i){
	if (i == 1) {
		if(velocidade > 0)
		velocidade--;
		return;
	}
	if (this->velocidade > 0 && this->energia > 0) {
		velocidade--;
	}
	if (this->velocidade > 0 && this->energia == 0) {
		velocidade--;
		velocidade--;
	}
	
}

int Carro::getVelocidade() const {
	return this->velocidade;
}

void Carro::velocidadeConstante() {
	this->velocidade = 1;
}

void Carro::setFinish(bool status){
	this->terminei = status;
}

void Carro::setPosicao(int posicao) {
	this->classificacao->setPosicao(posicao);
}