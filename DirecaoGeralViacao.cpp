#include "DirecaoGeralViacao.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;

DirecaoGeralViacao::DirecaoGeralViacao() {
	this->campeonato = new Campeonato();
	loadPilotos("piloto.txt");
	loadCarros("carro.txt");
	loadAutodromos("autodromo.txt");
}

DirecaoGeralViacao::~DirecaoGeralViacao(){
}

void DirecaoGeralViacao::loadPilotos(string filename){

	string line;
	ifstream file(filename);
	if (file.is_open()){
		while (getline(file, line)){
			stringstream   linestream(line);
			string name;
			string type;

			linestream >> type >> name;
			transform(type.begin(), type.end(), type.begin(), ::toupper);

			if (type == "CRAZY") {
				addCrazyDriver(name);
			}else if (type == "RAPIDO") {
				addPilotoRapido(name);
			}else if (type == "SURPRESA") {
				addPilotoSurpresa(name);
			}else if (type == "GENERICO") {
				addPilotoGenerico(name);
			}else {
				cout << "Nao foi possivel carregar o piloto " << name << ", tem um tipo invalido!" << "\n";
			}
		}
		file.close();
	}
}

void DirecaoGeralViacao::loadCarros(string filename) {

	string line;
	ifstream file(filename);
	if (file.is_open()) {
		while (getline(file, line)) {
			stringstream   linestream(line);
			string marca, modelo;
			float capMax, capInicial;

			linestream >> capInicial >> capMax >> marca >> modelo; 
			if (capMax <= 0 || capInicial <= 0) {
				cout << "Nao foi possivel carregar o carro " << marca << " " << modelo << "\n";
			}
			else {
				addCarro(marca, capInicial, capMax, modelo);
			}
		}
		file.close();
	}
}

void DirecaoGeralViacao::loadAutodromos(string filename){
	string line;
	ifstream file(filename);
	if (file.is_open()) {
		while (getline(file, line)) {
			stringstream   linestream(line);
			string nome;
			int cumprimento, capacidade;

			linestream >> capacidade >> cumprimento >> nome;

			if (cumprimento <= 0 || capacidade <= 0) {
				cout << "Nao foi possivel carregar o autodromo, parametros errados \n";
			}
			else {
				addAutodromo(cumprimento, capacidade, nome);
			}
			
		}
		file.close();
	}
}

bool DirecaoGeralViacao::addCarro(string marca, float capacidadeInicial, float capacidadeMaxima, string modelo) {
	if (capacidadeInicial <= 0 || capacidadeMaxima <= 0)
		return false;
	carros.push_back(new Carro(marca, capacidadeInicial, capacidadeMaxima, 120, modelo));
	return true;
}

bool DirecaoGeralViacao::addCarro(string marca, float capacidadeInicial, float capacidadeMaxima) {
	if (capacidadeInicial <= 0 || capacidadeMaxima <= 0)
		return false; 
	carros.push_back(new Carro(marca, capacidadeInicial, capacidadeMaxima, 120));
	return true;
}

void DirecaoGeralViacao::addCrazyDriver(string name) {
	new CrazyDriver(name, pilotos);
}
void DirecaoGeralViacao::addPilotoGenerico(string name) {
	new Piloto(name, pilotos);
}
void DirecaoGeralViacao::addPilotoRapido(string name) {
	new PilotoRapido(name, pilotos);
}
void DirecaoGeralViacao::addPilotoSurpresa(string name) {
	new PilotoSurpresa(name, pilotos);
}
int DirecaoGeralViacao::passaTempo() {
	return campeonato->passaTempo();
}


bool DirecaoGeralViacao::addAutodromo(int metros, int pistas, string nome) {
	if (metros <= 0 || pistas <= 0) {
		return false;
	}
	autodromos.push_back(new Autodromo(nome, pistas, metros, autodromos));
	return true;
}


bool  DirecaoGeralViacao::removeCarro(char c) {
	bool encontrou = false; 

	for (unsigned int j = 0; j < autodromos.size(); j++) {
		for (unsigned int i = 0; i < autodromos.at(j)->getCarrosPista().size(); i++) {
			if (autodromos.at(j)->getCarrosPista().at(i)->getIdentificador() == c) {
				autodromos.at(j)->removeCarro(autodromos.at(j)->getCarrosPista().at(i)->getIdentificador());
				encontrou = true;
			}
		}
		if (!encontrou) {
			for (unsigned int i = 0; i < autodromos.at(j)->getCarrosGaragem().size(); i++) {
				if (autodromos.at(j)->getCarrosGaragem().at(i)->getIdentificador() == c) {
					autodromos.at(j)->removeCarro(autodromos.at(j)->getCarrosGaragem().at(i)->getIdentificador());
					encontrou = true;
				}
			}
		}
	}

	if (encontrou) {
		for (unsigned int i = 0; i < carros.size(); i++)
			if (c == carros.at(i)->getIdentificador()) {
				delete carros.at(i);
				carros.erase(carros.begin() + i);
				return true;
			}
	}
	
	return false;
}


bool DirecaoGeralViacao::removePiloto(string nome) {
	for (unsigned int i = 0; i < carros.size(); i++) {
		if(carros.at(i)->getPiloto() != nullptr){
			if (carros.at(i)->getPiloto()->getNome() == nome) {
				if (carros.at(i)->getEstado() == EstadoCarro::MOVIMENTO)
					return false;
				carros.at(i)->removePiloto();
			}
		}
	}
		
	for (int j = 0; j < pilotos.size(); j++)
		if (pilotos.at(j)->getNome() == nome) {
			delete pilotos.at(j);
			pilotos.erase(pilotos.begin() + j);
			return true;
		}
	return false;
}

bool DirecaoGeralViacao::removeAutodromo(string nome) {
	for (unsigned int i = 0; i < autodromos.size(); i++)
		if (nome == autodromos.at(i)->getNome()) {
			delete autodromos.at(i);
			autodromos.erase(autodromos.begin() + i);
			return true;
		}
	return false;
			
}

bool DirecaoGeralViacao::removeCarroModoUm(char identificador) {
	for (unsigned int i = 0; i < carros.size(); i++)
		if (identificador == carros.at(i)->getIdentificador()) {
			delete carros.at(i);
			carros.erase(carros.begin() + i);
			return true;
		}
	return false;

}

bool DirecaoGeralViacao::inserePilotoNoCarro(char c, string nome) const {

	if (!contemPiloto(nome) || !contemCarro(c))
		return false;

	for (unsigned int i = 0; i < carros.size(); i++) { //vou verificar se o piloto est� em algum carro, se estiver nada acontece
		if(carros.at(i)->getPiloto() != nullptr)
			if (carros.at(i)->getPiloto()->getNome() == nome)
				return false;
	}
	
	//vai buscar o piloto para o meter no carro. A posicao do piloto fica guardada em j
	unsigned int j;
	for (j=0; j < pilotos.size(); j++)
		if (pilotos.at(j)->getNome() == nome)
			break;

	//o piloto existe, carro existe, e o piloto n�o est� em nenhum carro
	for (unsigned int i = 0; i < carros.size(); i++) {
		if (carros.at(i)->getIdentificador() == c) {
			if (carros.at(i)->getPiloto() != nullptr)
				saiDoCarro(c);
			carros.at(i)->setPiloto(pilotos.at(j));
			pilotos.at(j)->setEstado(EstadoPiloto::CARRO);
		}
	}
	carrosComPiloto++;
	return true;
}

bool DirecaoGeralViacao::saiDoCarro(char c) const {
	for (unsigned int i = 0; i < carros.size(); i++) {
		if (carros.at(i)->getIdentificador() == c) {
			if (carros.at(i)->getPiloto() == nullptr)
				return false;
			carros.at(i)->getPiloto()->setEstado(EstadoPiloto::SEMCARRO);
			carros.at(i)->removePiloto();
			carrosComPiloto--;
			return true;
		}
	}
	return false;
}

bool DirecaoGeralViacao::contemPiloto(string nome) const {
	
	for (unsigned int j = 0; j < pilotos.size(); j++) {
		if (pilotos.at(j)->getNome() == nome)
			return true;
	}
	return false;
}

bool DirecaoGeralViacao::contemCarro(char c) const {

	for (unsigned int j = 0; j < carros.size(); j++) {
		if (carros.at(j)->getIdentificador() == c)
			return true;
	}
	return false;
}


int DirecaoGeralViacao::getAutodromosSize() const {
	return (int)autodromos.size();
}


int DirecaoGeralViacao::getCarrosSize() const {
	return (int)carros.size();
}

int DirecaoGeralViacao::getPilotosSize() const
{
	return (int)pilotos.size();
}

int DirecaoGeralViacao::getPilotosEmCarros() const{
	int total = 0;
	for (int i = 0; i < pilotos.size(); i++) {
		if (pilotos.at(i)->isOnCar()) {
			total++;
		}
	}
	return total;
}

vector<Carro*> DirecaoGeralViacao::getCarrosVector() const{
	return carros;
}

Autodromo* DirecaoGeralViacao::getAutodromo(int pos) const {
	return autodromos.at(pos);
}

vector<Autodromo*> DirecaoGeralViacao::getAutodromosVector() const {
	return autodromos;
}

vector<Piloto*> DirecaoGeralViacao::getPilotosVector() const {
	return pilotos;
}

void DirecaoGeralViacao::carregaBaterias() const {
	for (unsigned int i = 0; i < carros.size(); i++) {
		carregaBaterias(carros.at(i)->getIdentificador(), carros.at(i)->getCapacidadeMaxima());
	}
}

void DirecaoGeralViacao::carregaBaterias(char c, float carga) const {
	for (unsigned int i = 0; i < carros.size(); i++) {
		if (carros.at(i)->getIdentificador() == c) {
			carros.at(i)->carregar(carga);
		}
	}
}

bool DirecaoGeralViacao::acidentaCarro(char c) const{
	bool encontrou = false;
	for (unsigned int i = 0; i < carros.size(); i++) {
		if (carros.at(i)->getIdentificador() == c) {
			carros.at(i)->setEstado(EstadoCarro::DANIFICADO);
			encontrou = true;
		}
	}

	if (encontrou) {
		for (unsigned int i = 0; i < autodromos.size(); i++) {
			for(unsigned int j = 0; j < autodromos.at(i)->getCarrosPista().size(); j++)
			if (autodromos.at(i)->getCarrosPista().at(j)->getIdentificador() == c) {
				autodromos.at(i)->saiPistaParaGaragem(autodromos.at(i)->getCarrosPista().at(j));
				return true;
			}
		}
	}

	return false;
}


void DirecaoGeralViacao::novoCampeonato(){
	this->campeonato = new Campeonato;
}

void DirecaoGeralViacao::apagaCampeonato(){
	for (int i = 0; i < campeonato->getAutodromoVector().size(); i++) {
		campeonato->getAutodromoVector().at(i)->limpaPista();
	}
	delete campeonato;
}

string DirecaoGeralViacao::getInfoCorrida() const {
	ostringstream info;
	Campeonato* campeonato = getCampeonato();
	Autodromo* autodromo = campeonato->getAutodromoAtual();
	if (autodromo->isCorridaADecorrer() == true) {
		vector<Carro*> pista = autodromo->getCarrosPista();
		for (size_t i = 0; i < pista.size(); i++){
			info << pista.at(i)->getAsString() << endl;
		}
	}
	return info.str();
}

string DirecaoGeralViacao::pilotosToString() const
{
	string ret;

	for (size_t i = 0; i < pilotos.size(); i++){
		ret += pilotos[i]->getNome() + " ";
	}
	return ret;
}

Autodromo * DirecaoGeralViacao::getAutodromo(string nome) const {
	for (int i = 0; i < autodromos.size(); i++) {
		if (getAutodromo(i)->getNome() == nome) {
			return getAutodromo(i);
		}
	}
	return nullptr;
}

Campeonato* DirecaoGeralViacao::getCampeonato() const {
	return campeonato;
}

bool DirecaoGeralViacao::addAutodromoAoCampeonato(string nome) {
	if (getAutodromo(nome) != nullptr) {
		if (getPilotosEmCarros() >= 2) {
			campeonato->addAutodromo(getAutodromo(nome));
			return true;
		}
	}
	return false;
}


Autodromo* DirecaoGeralViacao::getAutodromoAtual(){
	return campeonato->getAutodromoAtual();
}

int DirecaoGeralViacao::getCarrosComPiloto() const{
	return carrosComPiloto;
}

bool DirecaoGeralViacao::stopPiloto(string piloto) const {
	for (unsigned int i = 0; i < carros.size(); i++) {
		if (carros.at(i)->getPiloto()->getNome() == piloto) {
			carros.at(i)->getPiloto()->setStop();
			return true;
		}
	}
	return false;
}