#include "Campeonato.h"

Campeonato::Campeonato() {
	this->novoTerminou = false;
	this->terminados = 0;
	this->crazysParados = 0;
}

Campeonato::~Campeonato(){
	nCorridas = 0;
}

void Campeonato::addAutodromo(Autodromo* autodromo) {
	autodromos.push_back(autodromo);
}

void Campeonato::setNovoTerminou(bool status) {
	this->novoTerminou = status;
}
bool Campeonato::getNovoTerminou() const {
	return this->novoTerminou;
}

void Campeonato::removeAutodromo(Autodromo* autodromo) {
	for (unsigned int i = 0; i < autodromos.size(); i++) {
		if (autodromos.at(i) == autodromo && nCorridas > 0) {
			autodromos.erase(autodromos.begin() + i);
			nCorridas--;
			break;
		}
	}
}

vector<Autodromo*> Campeonato::getAutodromoVector() const {
	return autodromos;
}

Autodromo* Campeonato::getAutodromo(int pos) const {
	return autodromos.at(pos);
}

Autodromo * Campeonato::getAutodromoAtual() const {
	if (nCorridas < autodromos.size())
		return autodromos.at(nCorridas);
	else
		return autodromos.at(nCorridas-1);
}

int Campeonato::getSizeAutodromos() const {
	return (int)autodromos.size();
}

void Campeonato::setProxAutodromoACorrer(){
	nCorridas++;
}

int Campeonato::passaTempo() {
		Autodromo* autodromo = getAutodromoAtual();
		vector <Carro*> pista = autodromo->getCarrosPista();
		vector <Carro*> garagem = autodromo->getCarrosGaragem();

		for (size_t i = 0; i < pista.size(); i++) {
			if ((pista.at(i)->getPosicao() < autodromo->getMetros())) {
				int posicoes = pista.at(i)->getPiloto()->acelera(autodromo->getTempo(), i, pista.size(), pista.at(i));//se acao >= 0 tudo bem, se -1 carro com problemas
				if (posicoes == -1) { //danificado
					garagem.push_back(pista.at(i));
					pista.at(i)->setLugar(-1);
				}else {
					pista.at(i)->avancaPosicao();
				}
			}
			if (pista.at(i)->getPosicao() >= autodromo->getMetros() && !pista.at(i)->getFinish()) {
				pista.at(i)->setPosicao(autodromo->getMetros() + 1);
				autodromo->addVencedor(pista.at(i));
				pista.at(i)->setFinish(true);
				setNovoTerminou(true);
				terminados++;
			}
			if (pista.at(i)->getEstado() == EstadoCarro::CRAZYPARADO && !pista.at(i)->getFinish()) {
				pista.at(i)->setFinish(true);
				crazysParados++;
			}
		}
		if ((crazysParados + terminados) == pista.size()) {
			string nome = autodromo->getNome();
			autodromo->setCorridaADecorrer(false);
			autodromo->setVencedor(false);
			autodromo->setCorridaTerminada(true);
			setProxAutodromoACorrer();
			autodromo = getAutodromoAtual();
			if (autodromo->getNome() != nome) {
				autodromo->setCorridaADecorrer(true);
				clearVarCars();
			}
			else {
				return -1;
			}
		}
}

void Campeonato::clearVarCars() {
	Autodromo* autodromo = getAutodromoAtual();
	terminados = 0;
	crazysParados = 0;
	vector <Carro*> pista = autodromo->getCarrosPista();
	for (size_t i = 0; i < pista.size(); i++){
		pista.at(i)->limpa(i);
	}
}