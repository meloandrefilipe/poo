#ifndef CAMPEONATO_H
#define CAMPEONAT_H

#include <sstream>
#include "Autodromo.h"

using namespace std;
static int nCorridas = 0;

class Campeonato {
	vector<Autodromo *> autodromos;
	bool novoTerminou;
	int terminados;
	int crazysParados;
public:
	Campeonato();
	~Campeonato();
	void addAutodromo(Autodromo * autodromo); 
	void removeAutodromo(Autodromo* autodromo);
	vector<Autodromo*> getAutodromoVector() const;
	Autodromo* getAutodromoAtual() const;
	void setProxAutodromoACorrer();
	Autodromo* getAutodromo(int pos) const;
	int getSizeAutodromos() const;
	void apagaCampeonato();
	int passaTempo();
	void setNovoTerminou(bool status);
	bool getNovoTerminou() const;
	void clearVarCars();
};
#endif // !CAMPEONATO_H
