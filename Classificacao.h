#ifndef CLASSIFICACAO_H
#define CLASSIFICACAO_H

#include <iostream>
#include <vector>
#include <sstream>

using namespace std;

class Classificacao {
	int lugar; //pista numero x
	int posicao; //metros

public:
	Classificacao();
	~Classificacao();

	int getLugar() const;
	int getPosicao() const;

	void setLugar(int lugar);
	void setPosicao(int posicao);
	void avancaPosicao(int posicoes);
	void limpa(int i);
};

#endif 