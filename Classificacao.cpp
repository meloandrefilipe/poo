#include "Classificacao.h"

Classificacao::Classificacao(){
	this->lugar = 0;
	this->posicao = 0;
}

Classificacao::~Classificacao(){
}

int Classificacao::getLugar() const{
	return this->lugar;
}

int Classificacao::getPosicao() const{
	return this->posicao;
}

void Classificacao::setLugar(int lugar){
	this->lugar = lugar;
}

void Classificacao::setPosicao(int posicao) {
	this->posicao= posicao;
}

void Classificacao::avancaPosicao(int posicoes){
	this->posicao = this->posicao + posicoes;
}

void Classificacao::limpa(int i){
	this->lugar = i;
	this->posicao = 0;
}
