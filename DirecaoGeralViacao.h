#ifndef DGV_H
#define DGV_H


#include <iostream>
#include <vector>
#include <sstream>
#include <memory>
#include "Piloto.h"
#include "Carro.h"
#include "Autodromo.h"
#include "Campeonato.h"


using namespace std;
static int carrosComPiloto = 0;

class DirecaoGeralViacao{

	vector <Piloto*> pilotos;
	vector <Carro*> carros;
	vector <Autodromo*> autodromos;
	Campeonato* campeonato;

public:

	ostringstream log;

	DirecaoGeralViacao();
	~DirecaoGeralViacao();

	//gets
	int getAutodromosSize() const;
	int getCarrosSize() const;
	int getPilotosSize() const;
	int getPilotosEmCarros() const;
	int getCarrosComPiloto() const;
	vector<Carro*> getCarrosVector() const;
	vector<Autodromo*> getAutodromosVector() const;
	vector<Piloto*> getPilotosVector() const;
	Autodromo* getAutodromo(string nome) const;
	Autodromo* getAutodromoAtual();
	Autodromo* getAutodromo(int pos) const;
	Campeonato* getCampeonato() const;
	string getInfoCorrida() const;
	string pilotosToString() const;

	//func
	bool contemPiloto(string nome) const;
	bool contemCarro(char c) const;
	void carregaBaterias() const;
	void carregaBaterias(char c, float carga) const;
	bool acidentaCarro(char c) const;
	bool saiDoCarro(char c) const;
	bool stopPiloto(string piloto) const;
	void savedgv(string nome);
	int passaTempo();

	//load
	void loadPilotos(string filename);
	void loadCarros(string filename);
	void loadAutodromos(string filename);
	void loaddgv(string nome);

	//add
	bool addCarro(string marca, float capacidadeInicial, float capacidadeMaxima, string modelo); 
	bool addCarro(string marca, float capacidadeInicial, float capacidadeMaxima);
	void addCrazyDriver(string name);
	void addPilotoGenerico(string name);
	void addPilotoRapido(string name);
	void addPilotoSurpresa(string name);
	bool addAutodromoAoCampeonato(string nome);
	bool addAutodromo(int metros, int pistas, string nome);
	bool inserePilotoNoCarro(char c, string nome) const;
	void novoCampeonato();

	//remove
	bool removeCarro(char);
	bool removePiloto(string);
	bool removeAutodromo(string);
	bool removeCarroModoUm(char);
	void apagaCampeonato();
	void deldgv(string nome);

};

#endif //DGV