#include "Mapa.h"

Mapa::Mapa() {
	dgv = new DirecaoGeralViacao();
	mode = 1;
}

void Mapa::constroiMapa() {
	Autodromo* autodromo = dgv->getAutodromoAtual();
	int metros = autodromo->getMetros();
	int pistas = autodromo->getMaxCarros();
	Consola::clrscr();
	Sleep(100);
	Consola::setScreenSize(50+ pistas, metros +50);

	if (autodromo != nullptr) {
		for (int i = 0; i < metros + (20); i++) {
			Consola::gotoxy(6 + i, 1);
			cout << "_";
			Consola::gotoxy(6 + i, (pistas * 2) + 3);
			cout << "_";
		}
		for (int i = 0; i < pistas * 2 + 2; i++) {
			Consola::gotoxy(5, 2 + i);
			cout << "|";
			Consola::gotoxy(metros + 26, 2 + i);
			cout << "|";
		}

		int larguraPista = 50;
		int pista = 1;
		int i = 0;


		vector<Carro*> carros = autodromo->getCarrosPista();
		vector<Carro*> vencedores = autodromo->getVencedores();
		vector<Carro*> garagem = autodromo->getCarrosGaragem();

		for (size_t c = 0; c < carros.size(); c++){
			if (carros.at(c)->getPiloto() != nullptr) {
				if (carros.at(c)->getLugar() != -1) {
					Consola::gotoxy(16 + carros.at(c)->getPosicao(), 3 + carros.at(c)->getLugar() * 2);
					cout << carros.at(c)->getIdentificador();
				}
				else {
					Consola::gotoxy(16 + carros.at(c)->getPosicao(), 3 + carros.at(c)->getLugar() * 2);
					size_t j;
					for (j = 0; j < garagem.size(); j++)
						if (garagem.at(j)->getIdentificador() == carros.at(c)->getIdentificador())
							j = garagem.size();
					if(j==garagem.size()+1)
						garagem.push_back(carros.at(c));
				}
			}
		}

		for (i = 0; i < pistas; i++) {
			Consola::gotoxy(7, 3 + i * 2);
			cout << pista++;
			Consola::gotoxy(16, 4 + i * 2);
			larguraPista += 40;
			for (int j = 0; j < metros; j++) {
				cout << "-";
			}
		}

		Consola::gotoxy(0, 4 + i * 2);
		int posicaoFimGaragem = constroiGaragem(autodromo->getNome(), posCursor);
		posCursor = posicaoFimGaragem + i * 2;
		for (int i = 0; i < garagem.size(); i++) {
			Consola::gotoxy(0, posCursor++);
			cout << garagem.at(i)->getIdentificador();
		}
		if (vencedores.size() != 0 && dgv->getCampeonato()->getNovoTerminou()) {
			Consola::gotoxy(16, (pistas * 2) + 5);
			Consola::clrscr();
			if (vencedores.size() == 1) {
				cout << vencedores.at(0)->getIdentificador() << " ganhou a corrida. Prima uma tecla para continuar";
				Consola::getch();
				autodromo->setVencedor(true);
			}
			else {
				for (unsigned int j = 0; j < vencedores.size(); j++) {
					cout << vencedores.at(j)->getIdentificador() << ", ";
				}
				cout << " terminaram a corrida. Prima uma tecla para continuar";
				Consola::getch();
				Consola::clrscr();
			}
			dgv->getCampeonato()->setNovoTerminou(false);
			constroiMapa();
		}
	}
	else {
		Consola::gotoxy(0, posCursor++);
		cout << "O autodromo nao existe!";
	}
}

int Mapa::constroiGaragem(string nome, int posDoCursor) {
	posDoCursor++;
	cout << "Garagem" << endl;
	posDoCursor++;
	Autodromo* autodromo = dgv->getAutodromo(nome);
	for (unsigned j = 0; j < autodromo->getCarrosGaragem().size(); j++) {
		Consola::gotoxy(3, 4 + (posDoCursor++) * 2);
		cout << autodromo->getCarrosGaragem().at(j)->getIdentificador() << endl;
	}
	return posDoCursor;
}


void Mapa::constroiInterface() {

	Consola::setScreenSize(35, 160);
	while (true) {
		string comando;
		Consola::gotoxy(0, posCursor++);
		cout << "Comando: ";
		getline(cin, comando);
		trataComando(comando);
	}
}


int Mapa::getMode()
{
	return this->mode;
}

void Mapa::setMode(int mode)
{
	this->mode = mode;
}

void Mapa::trataComando(string comando) {
	istringstream buffer(comando);
	string aux;
	buffer >> aux;
	Autodromo* autodromo = nullptr;
	if (getMode() == 1) { // MODO 1
		if (aux == "carregaP") {
			buffer >> aux;
			if (fileExists(aux)) {
				dgv->loadPilotos(aux);
			}
			else {
				Consola::gotoxy(0, posCursor++);
				cout << "O  ficheiro " << aux << " nao existe!";
			}
		}
		else if (aux == "carregaC") {
			buffer >> aux;
			if (fileExists(aux)) {
				dgv->loadCarros(aux);
			}
			else {
				Consola::gotoxy(0, posCursor++);
				cout << "O  ficheiro " << aux << " nao existe!";
			}
		}
		else if (aux == "carregaA") {
			buffer >> aux;
			if (fileExists(aux)) {
				dgv->loadAutodromos(aux);
			}else {
				Consola::gotoxy(0, posCursor++);
				cout << "O  ficheiro " << aux << " nao existe!";
			}
		}
		else if (aux == "cria") {
			buffer >> aux;

			if (aux == "a") {
				int pistas;
				buffer >> pistas;

				int comprimento;
				buffer >> comprimento;

				buffer >> aux;
				if (!dgv->addAutodromo(comprimento, pistas, aux)) {
					Consola::gotoxy(0, posCursor++);
					cout << "Parametros errados";
				}
			}
			else if (aux == "c") {
				float capacidadeInicial;
				buffer >> capacidadeInicial;

				float capacidadeMaxima;
				buffer >> capacidadeMaxima;

				buffer >> aux;

				string modelo;
				buffer >> modelo;

				if (modelo == "")
					dgv->addCarro(aux, capacidadeInicial, capacidadeMaxima);
				else
					dgv->addCarro(aux, capacidadeInicial, capacidadeMaxima, modelo);
			}
			else if (aux == "p") {
				buffer >> aux;
				if (aux != "crazyDriver" && aux != "pilotoRapido" && aux != "pilotoSurpresa" && aux != "piloto") {
					Consola::gotoxy(0, posCursor++);
					cout << "Tipo de piloto n�o existe!";
				}
				else {
					string auxNome;
					buffer >> auxNome;
					string nome = auxNome;
					string anterior;
					do {
						anterior = auxNome;
						buffer >> auxNome;
						if (auxNome != anterior)
							nome = nome + " " + auxNome;
					} while (auxNome != anterior);

					if (aux == "crazyDriver") {
						dgv->addCrazyDriver(nome);
					} else if (aux == "pilotoRapido") {
						dgv->addPilotoRapido(nome);
					}else if (aux == "pilotoSurpresa") {
						dgv->addPilotoSurpresa(nome);
					}else{
						dgv->addPilotoGenerico(nome);
					}
				}
			}
			else {
				Consola::gotoxy(0, posCursor++);
				cout << "O parametro " << aux << " nao existe!";
			}
		}
		else if (aux == "apaga") {
			buffer >> aux;

			if (aux == "c") {
				char identificador;
				buffer >> identificador;

				if (!dgv->removeCarroModoUm(identificador)) {
					Consola::gotoxy(0, posCursor++);
					dgv->log << "O carro " << identificador << " nao existe!   |   ";
					cout << "O carro " << identificador << " nao existe!";
				}
			}
			else if (aux == "a") {
				buffer >> aux;


				if (!dgv->removeAutodromo(aux)) {
					Consola::gotoxy(0, posCursor++);
					dgv->log << "O autodromo " << aux << " nao existe!   |   ";
					cout << "O autodromo " << aux << " nao existe!";
				}
			}
			else if (aux == "p") {
				string auxNome;
				buffer >> auxNome;
				string nome = auxNome;
				string anterior;
				do {
					anterior = auxNome;
					buffer >> auxNome;
					if (auxNome != anterior)
						nome = nome + " " + auxNome;
				} while (auxNome != anterior);

				if (!dgv->removePiloto(nome)) {
					Consola::gotoxy(0, posCursor++);
					dgv->log << "O piloto " << aux << " nao existe!   |   ";
					cout << "O piloto " << aux << " nao existe!";
				}
			}
			else {
				Consola::gotoxy(0, posCursor++);
				cout << "O parametro " <<aux << " nao existe!";
			}
		}
		else if (aux == "entranocarro") {
			char identificador;
			buffer >> identificador;

			string auxNome;
			buffer >> auxNome;
			string nome = auxNome;
			string anterior;
			do {
				anterior = auxNome;
				buffer >> auxNome;
				if (auxNome != anterior)
					nome = nome + " " + auxNome;
			} while (auxNome != anterior);
			if (!dgv->inserePilotoNoCarro(identificador, nome)) {
				Consola::gotoxy(0, posCursor++);
				cout << "Carro ou piloto nao existe";
			}

		}
		else if (aux == "saidocarro") {
			char identificador;
			buffer >> identificador;

			if (!dgv->saiDoCarro(identificador)) {
				Consola::gotoxy(0, posCursor++);
				cout << "O carro nao existe ou nao possui piloto";
			}
		}
		else if (aux == "lista") { 
			lista();
		}
		else if (aux == "campeonato") {
			dgv->novoCampeonato();
			string anterior;
			while (true) {
				anterior = aux;
				buffer >> aux;

				if (anterior == aux) {
					break;
				}
				if (!dgv->addAutodromoAoCampeonato(aux)) {
					Consola::gotoxy(0, posCursor++);
					if (dgv->getPilotosEmCarros() < 2) {
						cout << "Tem de existir pelo menos 2 pilotos nos carros" << endl;
					}
					else {
						cout << "Nao foi possivel adicionar o autodromo " << aux << " ao campeonato!" << endl;
					}
				}
				else {
					for (int i = 0; i < dgv->getCampeonato()->getSizeAutodromos(); i++) {
						Autodromo* autodromo = dgv->getCampeonato()->getAutodromo(i);
						if (autodromo->getNome() == aux) {
							for (size_t j = 0; j < dgv->getCarrosSize(); j++) {
								if (dgv->getCarrosVector().at(j)->getPiloto() != nullptr) {
									if (autodromo->getTotalCarrosPista() < autodromo->getMaxCarros()) {
										autodromo->addCarroPista(dgv->getCarrosVector().at(j));
									}
								}
							}
						}
					}
					setMode(2);
				}
			}
		}
		else if (aux == "clear") {
			Consola::clrscr();
			posCursor = 0;

		}
		else if (aux == "sair") {
			exit(EXIT_SUCCESS);
		}
		else {
			Consola::gotoxy(0, posCursor++);
			cout << "Comando nao existe!";
		}
	} else if(getMode() == 2) { //MODO 2
		if (aux == "listacarros") {
			listaCarros();
		}
		else if (aux == "carregabat") {
			char identificador;
			buffer >> identificador;

			float carga;
			buffer >> carga;

			dgv->carregaBaterias(identificador, carga);
		}
		else if (aux == "carregatudo") {
			dgv->carregaBaterias();
		}
		else if (aux == "corrida") {
			autodromo = dgv->getAutodromoAtual();
			autodromo->setCorridaADecorrer(true);
			int capacidade = autodromo->getMaxCarros();
			if (capacidade > dgv->getCarrosComPiloto()) {
				capacidade = dgv->getCarrosComPiloto();
			}
			vector<Carro*> pista = autodromo->getCarrosPista();
			for (int i = 0; i < dgv->getCarrosSize(); i++) {
				if (capacidade > 0) {
					if (dgv->getCarrosVector().at(i)->getPiloto() != nullptr) {
						pista.push_back(dgv->getCarrosVector().at(i));
						pista.at(i)->setLugar(i);
					}
					capacidade--;
				}
				else {
					break;
				}
			}
			constroiMapa();
		}
		else if (aux == "acidente") {
			char identificador;
			buffer >> identificador;

			if (!dgv->acidentaCarro(identificador)) {
				Consola::gotoxy(0, posCursor++);
				dgv->log << "O carro " << identificador << " nao existe ou ja esta acidentado!   |   ";
				cout << "O carro " << identificador << " nao existe ou ja esta acidentado!";
			}
		}
		else if (aux == "destroi") {
			char identificador;
			buffer >> identificador;

			if (!dgv->removeCarro(identificador)) {
				Consola::gotoxy(0, posCursor++);
				dgv->log << "O carro " << identificador << " nao existe!   |   ";
				cout << "O carro " << identificador << " nao existe!";
			}
		}
		else if (aux == "stop") {
			string auxNome;
			buffer >> auxNome;
			string nome = auxNome;
			string anterior;
			do {
				anterior = auxNome;
				buffer >> auxNome;
				if (auxNome != anterior)
					nome = nome + " " + auxNome;
			} while (auxNome != anterior);

			if (!dgv->stopPiloto(nome)) {
				Consola::gotoxy(0, posCursor++);
				dgv->log << "O  piloto " << nome << " nao existe!   |   ";
				cout << "O  piloto " << nome << " nao existe!";
			}
		}
		else if (aux == "passatempo") {
			autodromo = dgv->getAutodromoAtual();
			int tempo;
			buffer >> tempo;
			if (autodromo->getCorridaTerminada()) {
				Consola::gotoxy(0, posCursor++);
				cout << "A corrida terminou" << endl;
				return;
			}
			if (tempo > 0 && autodromo != nullptr) {
				if (tempo > autodromo->getMetros()) {
					tempo = autodromo->getMetros();
				}
				for (int i = 0; i < tempo; i++) {
					if (dgv->passaTempo() == -1) {
						Consola::getch();
						constroiMapa();
						break;
					}
					Consola::getch();
					constroiMapa();
				}
			}
		}
		else if (aux == "log") {
			Consola::gotoxy(0, posCursor++);
			cout << dgv->log.str();
			dgv->log.str("");
			dgv->log.clear();
		}
		else if (aux == "sair") {
			autodromo = dgv->getAutodromoAtual();
			autodromo->setCorridaADecorrer(false);
			autodromo->setCorridaTerminada(false);
			autodromo->setVencedor(false);
			autodromo->limpaClassificacoes();
			dgv->apagaCampeonato(); 
			Consola::clrscr();
			posCursor = 0;
			setMode(1);
		}
		else if (aux == "clear") {
			Consola::clrscr();
			posCursor = 0;
		}
		else {
			Consola::gotoxy(0, posCursor++);
			cout << "Comando nao existe!";
		}
	}
	return;
}

bool Mapa::fileExists(const string& name)
{
	ifstream f(name.c_str());
	return f.good();
}

void Mapa::listaCarros() {
		Consola::gotoxy(0, posCursor++);
		cout << dgv->getInfoCorrida() << endl;
		posCursor = posCursor + dgv->getAutodromoAtual()->getTotalCarrosPista();
}

void Mapa::lista() {

	if (dgv->getAutodromosSize() == 0) {
		Consola::gotoxy(0, posCursor++);
		cout << "Sem autodromos" << endl;
	}
	else {
		posCursor++;
		Consola::gotoxy(0, posCursor++);
		cout << "---Autodromos---" << endl;
		posCursor++;
		for (int i = 0; i < dgv->getAutodromosSize(); i++) {
			Consola::gotoxy(0, posCursor++);
			cout << dgv->getAutodromosVector().at(i)->getAsString() << endl;
		}
		posCursor++;
	}

	if (dgv->getCarrosSize() == 0) {
		Consola::gotoxy(0, posCursor++);
		cout << "Sem carros" << endl;
	}
	else {
		posCursor++;
		Consola::gotoxy(0, posCursor++);
		cout << "---Carros---" << endl;
		posCursor++;
		for (int i = 0; i < dgv->getCarrosSize(); i++) {
			Consola::gotoxy(0, posCursor++);
			cout << dgv->getCarrosVector().at(i)->getAsString2() << endl;
		}
		posCursor++;
	}

	if (dgv->getPilotosSize() == 0) {
		Consola::gotoxy(0, posCursor++);
		cout << "Sem pilotos" << endl;
	}
	else {
		posCursor++;
		Consola::gotoxy(0, posCursor++);
		cout << "---Pilotos---" << endl;
		posCursor++;
		for (unsigned int i = 0; i < dgv->getPilotosVector().size(); i++) {
			Consola::gotoxy(0, posCursor++);
			cout << dgv->getPilotosVector().at(i)->getAsString() << endl;
		}
		posCursor++;
	}
}
