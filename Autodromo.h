#ifndef AUTODROMO_H
#define AUTODROMO_H
#include "Carro.h"
#include <sstream>
#include <map>

static int REPETIDOS = 0;
static int TERMINADOS = 0;

using namespace std;


class Autodromo {
	vector <Carro*> pista;
	vector <Carro*> vencedores;
	vector <Carro*> garagem;
	string nome;
	int numeroMaximoCarros;
	int metros;
	bool corridaADecorrer;
	bool temVencedor;
	int tempo;
	bool corridaTerminada;
public:
	Autodromo(string nome, int max, int metros, vector<Autodromo*>&lista);
	~Autodromo(); 

	//gets
	string getNome() const;
	int getMetros() const;
	int getMaxCarros() const;
	int getPosicao(Carro* carro) const;
	int getTotalCarrosGaragem() const;
	int getTotalCarrosPista() const ;
	int getVencedoresSize() const;
	string getAsString() const;
	vector<Carro*> getCarrosGaragem() const;
	vector <Carro*> getCarrosPista() const;
	bool getVencedor() const;
	int getTempo() const;
	vector<Carro*> getVencedores() const;
	bool getCorridaTerminada() const;

	//sets
	void setCorridaADecorrer(bool res);
	void setVencedor(bool);
	void incrementaTempo();
	void setCorridaTerminada(bool status);


	//func
	bool saiGaragemParaPista(char ident);
	bool saiPistaParaGaragem(Carro* carro);
	void addCarroPista(Carro * carro);
	void addVencedor(Carro* carro);
	bool isCorridaADecorrer() const;
	void addCarroGaragem(Carro* carro);
	void removeCarroGaragem(Carro* carro);
	void limpaClassificacoes();
	void limpaPista();
	void removeCarro(char nome);
};


#endif //AUTODROMO_H
