#include "Autodromo.h"
using namespace std;

Autodromo::Autodromo(string nome, int max, int metros, vector<Autodromo*>&lista) {
	bool existe = false;
	for (unsigned int i = 0; i < lista.size(); i++) {
		if (lista.at(i)->getNome() == nome) {
			existe = true;
		}
	}
	if (!existe){
		this->nome = nome;
	}else{
        this->nome = nome + to_string(REPETIDOS);
        REPETIDOS++;
	}
	this->metros = metros;
    this->numeroMaximoCarros = max;
	this->corridaADecorrer = false;
	this->temVencedor = false;
	this->tempo = 0;
}

Autodromo::~Autodromo() {
	for (unsigned int i = 0; i < pista.size(); i++) {
		pista.at(i)->setEstado(EstadoCarro::DANIFICADO);
		pista.at(i)->removePiloto();
		pista.erase(pista.begin() + i);
	}
	for (unsigned int i = 0; i < garagem.size(); i++) {
		garagem.at(i)->setEstado(EstadoCarro::DANIFICADO);
		garagem.at(i)->removePiloto();
		garagem.erase(garagem.begin() + i);
	}

	delete this;
}

string Autodromo::getNome() const {
	return this->nome;
}

bool Autodromo::isCorridaADecorrer() const {
	return this->corridaADecorrer;
}

void Autodromo::setCorridaADecorrer(bool res) {
	this->corridaADecorrer = res;
}

int Autodromo::getMetros() const {
	return this->metros;
}

int Autodromo::getMaxCarros() const {
	return this->numeroMaximoCarros;
}

int Autodromo::getPosicao(Carro* carro) const{
	return carro->getPosicao();
}

bool Autodromo::saiGaragemParaPista(char ident) {
	for (unsigned int j = 0; j < this->garagem.size(); j++) {
		if (ident == garagem.at(j)->getIdentificador() && garagem.at(j)->getPiloto() != nullptr) {
			garagem.at(j)->setEstado(EstadoCarro::MOVIMENTO);
			pista.push_back(garagem.at(j));
			garagem.erase(garagem.begin() + j);
			return true;
		}
	}
	return false;
}

bool Autodromo::saiPistaParaGaragem(Carro * carro) {
	for (unsigned int j = 0; j < this->pista.size(); j++) {
		if (carro->getIdentificador() == pista.at(j)->getIdentificador()) {
			if(pista.at(j)->getEstado() == EstadoCarro::MOVIMENTO)
				pista.at(j)->setEstado(EstadoCarro::PARADO);
			garagem.push_back(pista.at(j));
			pista.erase(pista.begin() + j);
			return true;
		}
	}
	return false;
}


vector<Carro*> Autodromo::getCarrosGaragem() const {
	return garagem;
}
int Autodromo::getTotalCarrosGaragem() const {
	return (int)garagem.size();
}

vector<Carro*> Autodromo::getCarrosPista() const {
	return pista;
}

int Autodromo::getTotalCarrosPista() const {
	return (int)pista.size();
}
int Autodromo::getVencedoresSize() const {
	return (int)vencedores.size();
}

void Autodromo::addCarroGaragem(Carro* carro){
	garagem.push_back(carro);
}
void Autodromo::addCarroPista(Carro* carro) {
	pista.push_back(carro);
}
void Autodromo::addVencedor(Carro* carro) {
	vencedores.push_back(carro);
}

void Autodromo::removeCarroGaragem(Carro* carro){
	for (unsigned int i = 0; i < garagem.size(); i++) {
		if (garagem.at(i)->getIdentificador() == carro->getIdentificador()) {
			garagem.erase(garagem.begin() + i);
		}
	}
}

void Autodromo::limpaClassificacoes(){
	for (size_t i = 0; i < pista.size(); i++){
		pista.at(i)->limpa(i);
	}
}

void Autodromo::limpaPista() {
	setVencedor(false);
	setCorridaADecorrer(false);
	setCorridaTerminada(false);
	for (size_t i = 0; i < pista.size(); ) {
		pista.erase(pista.begin() + i);
	}

	for (size_t i = 0; i < garagem.size(); ) {
		garagem.erase(garagem.begin() + i);
	}

	for (size_t i = 0; i < vencedores.size(); ) {
		vencedores.erase(vencedores.begin() + i);
	}
}

string Autodromo::getAsString() const {
	ostringstream str;
	str << "Autodromo " << this->getNome() << ". Possui " << this->getCarrosGaragem().size() << " carros em garagem e " << this->getCarrosPista().size() << " carros em pista. Tem " << this->getMetros() << " metros e " << this->getMaxCarros() << " pistas." ;
	return str.str();
}

void Autodromo::setVencedor(bool res) {
	this->temVencedor = res;
}

bool Autodromo::getVencedor() const {
	return this->temVencedor;
}

int Autodromo::getTempo() const {
	return this->tempo;
}

void Autodromo::incrementaTempo() {
	this->tempo++;
}

void Autodromo::removeCarro(char c) {
	for (unsigned int i = 0; i < pista.size(); i++) {
		if (c == pista.at(i)->getIdentificador()) {
			pista.erase(pista.begin() + i);
			return;
		}
	}
	
	for (unsigned int i = 0; i < garagem.size(); i++) {
		if (c == garagem.at(i)->getIdentificador()) {
			garagem.erase(garagem.begin() + i);
			return;
		}
	}
}

vector<Carro*> Autodromo::getVencedores() const {
	return this->vencedores;
}

void Autodromo::setCorridaTerminada(bool status) {
	this->corridaTerminada = status;
}
bool Autodromo::getCorridaTerminada() const {
	return this->corridaTerminada;
}