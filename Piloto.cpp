#include "Carro.h"
#include "Piloto.h"
using namespace std;


Piloto::Piloto(string nome, vector<Piloto *> &lista){
	bool unique = true;
	for (unsigned int i = 0; i < lista.size(); i++) {
		if (lista.at(i)->getNome() == nome) {
			unique = false;
			PILOTOS_REPETIDOS++;
			break;
		}
	}
	if (!unique) {
		this->nome = nome + to_string(PILOTOS_REPETIDOS);
	}
	else {
		this->nome = nome;
	}
	this->estado = EstadoPiloto::SEMCARRO;
	this->tipo = TipoPiloto::GENERICO;
	this->stop = false;
	lista.push_back(this);
}

Piloto::~Piloto() {}
CrazyDriver::~CrazyDriver() {}
PilotoRapido::~PilotoRapido() {}
PilotoSurpresa::~PilotoSurpresa() {}




string Piloto::getNome() const {
	return this->nome;
}

EstadoPiloto Piloto::getEstado() const {
	return this->estado;
}

TipoPiloto Piloto::getTipo() const {
	return this->tipo;
}

string Piloto::getEstadoText() const {
	ostringstream os;
	switch (this->getEstado()) {
	case EstadoPiloto::CARRO:
		os << "Piloto esta num carro.";
		break;
	case EstadoPiloto::SEMCARRO:
		os << "Piloto sem carro.";
		break;
	}
	return os.str();
}

string Piloto::getTipoText() const {
	ostringstream os;
	switch (this->getTipo()) {
	case TipoPiloto::CRAZY:
		os << "Crazy driver";
		break;
	case TipoPiloto::RAPIDO:
		os << "Rapido";
		break; 
	case TipoPiloto::GENERICO:
		os << "Piloto normal";
		break; 
	case TipoPiloto::SURPRESA:
		os << "Surpresa";
			break;
	}
	return os.str();
}

string Piloto::getAsString() const {
	ostringstream str;
	str << "Piloto " << this->getNome() << ". Tipo: " << this->getTipoText() << ". Estado: " << this->getEstadoText();
	return str.str();
}

void Piloto::setEstado(EstadoPiloto e){
	this->estado = e;
}

int CrazyDriver::calculaSegundosDeAtraso(int segundos) {
	if (segundos == 0) {
		this->segundosEmAtraso = rand() % 5 + 1;
	}
	if (segundosEmAtraso > 0) {
		this->segundosEmAtraso--;
		return -1;
	}
	return 0;
}

bool CrazyDriver::calculaProbabilidadeDanificado() const {
	int r = rand() % 1 + 100;
	return r <= 5;
}

bool PilotoRapido::calculaAnsiedade() const {
	int r = rand() % 1 + 100;
	return r <= 10;
}

bool Piloto::isOnCar() const {
	switch (this->getEstado()) {
		case EstadoPiloto::CARRO:
			return true;
		case EstadoPiloto::SEMCARRO:
			return false;
	}
	return false;
}


int CrazyDriver::acelera(int segundo, int posicao, int carrosEmPista, Carro *carro) {
	if (this->stop) {
		stopCarro(carro);
		return carro->getVelocidade();
	}

	if (calculaProbabilidadeDanificado()) {
		carro->setEstado(EstadoCarro::DANIFICADO);
		return -1;
	}
	if (calculaSegundosDeAtraso(segundo) == -1) { //est� distraido
		carro->setEstado(EstadoCarro::CRAZYPARADO);
		return 0;
	}
	if (posicao == 1) {//vai em primeiro, mantem a velocidade
		carro->gastaEnergia();
		return carro->getVelocidade();
	}
	if (posicao == carrosEmPista) { //vai em ultimo, carrega no travao pq amua
		carro->trava(0);
		carro->gastaEnergia();
		carro->setEstado(EstadoCarro::CRAZYPARADO);
		return carro->getVelocidade();
	}
	//simplesmente acelera
	carro->acelera();
	carro->acelera();
	carro->gastaEnergia();
	return carro->getVelocidade();
}


int PilotoRapido::acelera(int segundo, int posicao, int carrosEmPista, Carro * carro) {
	
	if (this->stop) {
		stopCarro(carro);
		return carro->getVelocidade();
	}

	if (calculaAnsiedade() && (segundo % 10) == 0)
		return -1;

	if (carro->getEnergia() > carro->getCapacidadeMaxima() / 2) { //ate gastar metade da energia acelera
		carro->acelera();
		carro->gastaEnergia();
		return carro->getVelocidade();
	}		

	if (this->contadorSegundos < 3) {
		this->contadorSegundos++;
		carro->gastaEnergia();
		return carro->getVelocidade();
	}
		//passaram-se os 3 segundos
		carro->acelera();
		carro->gastaEnergia();
		contadorSegundos = 0;
		return carro->getVelocidade();
}

int PilotoSurpresa::acelera(int segundo, int posicao, int carrosEmPista, Carro* carro) {
	if (this->stop) {
		stopCarro(carro);
		return carro->getVelocidade();
	}
		
	if (segundo == 0) {
		carro->acelera();
		carro->gastaEnergia();
		this->contadorSegundos++;
		return carro->getVelocidade();
	}

	if (this->contadorSegundos == 0) {
		carro->trava(0);
		carro->gastaEnergia();
		this->contadorSegundos++;
		return carro->getVelocidade();
	}
		
	if (this->contadorSegundos == 3) {
		for(int i=0; i< this->contadorSegundos; i++)
			carro->acelera(); 
		carro->gastaEnergia();
		this->contadorSegundos = 0;
		return carro->getVelocidade();
	}

	carro->gastaEnergia();
	this->contadorSegundos++;
	return carro->getVelocidade();
}


int Piloto::acelera(int segundo, int posicao, int carrosEmPista, Carro* carro) {
	
	if (this->stop) {
		stopCarro(carro);
		return carro->getVelocidade();
	}
	carro->velocidadeConstante();
	carro->gastaEnergia();
	return 1;
}

void Piloto::stopCarro(Carro *carro) {
	carro->trava(1);
}

bool Piloto::getStop() const {
	return this->stop;
}

void Piloto::setStop() {
	this->stop = !this->stop;
}